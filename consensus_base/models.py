# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import datetime
import logging
import pytz
import uuid
import time
from jsoneditor.fields.django_jsonfield import JSONField

from django.db import models
from django.conf import settings as s

from .utils import unique_slugify

logger = logging.getLogger(__name__)


class Poll(models.Model):
    creatorid = models.IntegerField(blank=True, null=True)
    creator = models.CharField(blank=True, null=True, max_length=250)
    title = models.CharField(max_length=1000)
    slug = models.SlugField(blank=True)
    uuid = models.UUIDField(default=uuid.uuid4, editable=False)
    description = models.TextField(max_length=10000)
    published = models.BooleanField(default=False)
    locked = models.BooleanField(default=False)
    public = models.BooleanField(default=False)
    startdate = models.DateTimeField(
        null=True, blank=True, db_index=True
    )
    enddate = models.DateTimeField(
        null=True, blank=True, db_index=True
    )

    def __str__(self):
        return self.title

    def save(self, **kwargs):
        slug_str = "%s" % (self.title)
        unique_slugify(self, slug_str)
        if not self.uuid:
            self.uuid = uuid4()
        super(Poll, self).save(**kwargs)


class PollChange(models.Model):
    uuid = models.UUIDField(default=uuid.uuid4, editable=False)
    poll = models.ForeignKey(Poll)
    change = JSONField(
        blank=True,
        default= {},
    )
    ctime = models.DateTimeField(
            null=True, blank=True, db_index=True
    )

    def __str__(self):
        return self.uuid.hex

    def save(self, **kwargs):
        if not self.ctime:
            self.ctime = datetime.datetime.fromtimestamp(time.time(), pytz.UTC)
        if not self.uuid:
            self.uuid = uuid4()
        super(PollChange, self).save(**kwargs)


class PollParticipant(models.Model):
    username = models.CharField(max_length=250)
    poll = models.ForeignKey(Poll)
    admin = models.BooleanField(default=False)


class PollGroupParticipant(models.Model):
    group = models.CharField(max_length=250)
    poll = models.ForeignKey(Poll)


class PollQuestion(models.Model):
    question = models.CharField(max_length=1000)
    slug = models.SlugField(blank=True)
    uuid = models.UUIDField(default=uuid.uuid4, editable=False)
    help = models.TextField(max_length=10000)
    poll = models.ForeignKey(Poll)
    typeofquestion = models.CharField(
        default=getattr(s, 'CONSENSUS_POLL_QUESTION_TYPE_DEFAULT', ""),
        choices=getattr(s, 'CONSENSUS_POLL_QUESTION_TYPE_CHOICES', ()),
        max_length=250,
    )
    hide = models.BooleanField(default=False)
    mandatory = models.BooleanField(default=False)
    properties = JSONField(
        blank=True,
        default=[
            {"Property (ex: Max. Weight)": "Value (ex: 100)"},
        ]
    )
    order = models.IntegerField(default=0)
    choices = JSONField(
        blank=True,
        default={
            1: "Not at all",
            2: "Not very interesting",
            3: "Interesting",
            4: "Very interesting",
            5: "It is what we need",
            6: "Abstention",
        }
    )

    def __str__(self):
        return self.question

    def save(self, **kwargs):
        slug_str = "%s" % (self.question)
        unique_slugify(self, slug_str)
        if not self.uuid:
            self.uuid = uuid4()
        super(PollQuestion, self).save(**kwargs)


class PollQuestionAnswer(models.Model):
    question = models.ForeignKey(PollQuestion)
    mtime = models.DateTimeField(
            auto_now_add=True, null=True, blank=True, db_index=True
    )
    userid = models.IntegerField()
    username = models.CharField(max_length=250)

    answer = JSONField(
        blank=True,
        default={
            "1": "Choice 1",
            "2": "Choice 2",
        }
    )

    def __str__(self):
        return "[Poll Question Answer] [%s] [%s]" % (self.question,
                                                     self.username)

    def save(self, **kwargs):
        d = datetime.datetime.fromtimestamp(time.time(), pytz.UTC)
        if not self.mtime:
            self.mtime = d

        super(PollQuestionAnswer, self).save(**kwargs)


class PollQuestionComment(models.Model):
    question = models.ForeignKey(PollQuestion)
    mtime = models.DateTimeField(
            auto_now_add=True, null=True, blank=True, db_index=True
    )
    userid = models.IntegerField()
    username = models.CharField(max_length=250)
    uuid = models.UUIDField(default=uuid.uuid4, editable=False)
    comment = models.TextField(max_length=10000)

    def __str__(self):
        return "[Poll Question Comment] [%s] [%s]" % (self.question,
                                                      self.username)

    def save(self, **kwargs):
        d = datetime.datetime.fromtimestamp(time.time(), pytz.UTC)
        if not self.mtime:
            self.mtime = d
        if not self.uuid:
            self.uuid = uuid4()

        super(PollQuestionComment, self).save(**kwargs)
