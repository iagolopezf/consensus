from django.conf import settings as s


def settings(request):
    context_data = dict()
    context_data['logo_url'] = getattr(s, "CONSENSUS_LOGO_URL", None)
    context_data['logout_url'] = getattr(s, "CONSENSUS_LOGOUT_URL", None)
    context_data['support_url'] = getattr(s, "CONSENSUS_SUPPORT_URL", None)
    context_data['profile_url'] = getattr(s, "CONSENSUS_PROFILE_URL", None)
    context_data['MEDIA_URL'] = getattr(s, "MEDIA_URL", None)
    return context_data
