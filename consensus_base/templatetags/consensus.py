from django import template
from ..helpers import is_admin
from ..helpers import get_choices_status
from ..helpers import get_num_of_questions_total
from ..helpers import get_participants
from ..helpers import get_participants_pending
from ..helpers import get_questions_pending
from ..helpers import get_question_participants
from ..models import Poll
from ..models import PollQuestion
 
# from django.conf import settings as s

register = template.Library()


@register.filter
def getitem(dictionary, key):
    return dictionary.get(key)


@register.filter('startswith')
def startswith(text, starts):
    if isinstance(text, str):
        return text.startswith(starts)
    return False


@register.filter('ispolladmin')
def ispolladmin(poll, username):
    return is_admin(poll, username)


@register.filter('pendingparticipants')
def questionparticipants(question):
    return get_question_participants(question)


@register.filter('pendingparticipants')
def pendingparticipants(poll):
    return get_participants_pending(poll.id)[1]


@register.filter('numofpendingvotes')
def numofpendingvotes(poll):
    return get_participants_pending(poll.id)[0]


@register.filter('pendingvotes')
def pendingvotes(poll):
    return get_participants_pending(poll.id)[1]


@register.filter('numofparticipants')
def numofparticipants(poll):
    return get_participants(poll.id)[0]


@register.filter('participants')
def participants(poll):
    return get_participants(poll.id)[1]


@register.filter('pollnumofvotes')
def pollnumofvotes(poll):
    return get_participants(poll.id)[0] - get_participants_pending(poll.id)[0]


@register.filter('pollvotes')
def pollvotes(poll):
    pending_participants = get_participants_pending(poll.id)[1]
    participants = get_participants(poll.id)[1]
    return list(filter(lambda x: x not in pending_participants, participants))


@register.filter('pendingquestions')
def pendingquestions(poll, username):
    return get_questions_pending(poll.id, username)[0]


@register.filter('totalquestions')
def totalquestions(poll):
    return get_num_of_questions_total(poll.id)


@register.filter('pollquestions')
def pollquestions(poll):
    return PollQuestion.objects.filter(hide=False,poll=poll).order_by('id')


@register.filter('get')
def get(iterable_object, key):
    return iterable_object[key]


@register.filter('listofvotes')
def listofvotes(question, choice):
    return get_choices_status(question, userid=None)[choice]["list_of_participants"]


@register.filter('numofvotes')
def numofvotes(question, choice):
    return get_choices_status(question, userid=None)[choice]["num_of_answers"]


@register.filter('comments')
def comments(question, orderby='mtime'):
    return question.pollquestioncomment_set.order_by(orderby)
