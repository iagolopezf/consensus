from django.conf.urls import url
from django.views.generic.base import RedirectView

from . import views

urlpatterns = [
    url(r'^$', RedirectView.as_view(url='polls',
                                    permanent=False), name='index'),
    url(r'^poll/create$', views.poll_create, name="poll_create"),
    url(r'^poll/(?P<slug>[-\w]+)$', views.poll, name="poll"),
    url(r'^poll/(?P<slug>[-\w]+)/delete$',
        views.poll_delete, name="poll_delete"),
    url(r'^poll/(?P<slug>[-\w]+)/edit$',
        views.poll_edit, name="poll_edit"),
    url(r'^poll/(?P<slug>[-\w]+)/formatted$',
        views.poll_formatted, name="poll_formatted"),
    url(r'^poll/(?P<slug>[-\w]+)/settings',
        views.poll_settings, name="poll_settings"),
    url(r'^poll/(?P<uuid>[-\w]+)/action/(?P<action>[-\w]+)',
        views.poll_action, name="poll_action"),
    url(r'^polls$', views.polls, name="polls"),
    url(r'^json/answer$',
        views.json_answer, name="json_answer"),
    url(r'^json/comment$',
        views.json_comment, name="json_comment"),
    url(r'^json/poll/(?P<uuid>[-\w]+)/status$',
        views.json_poll_status, name="json_poll_status"),
    url(r'^json/poll/(?P<uuid>[-\w]+)/changes$',
        views.json_poll_changes, name="json_poll_changes"),
    url(r'^json/question$',
        views.json_question, name="json_question"),
    url(r'^json/choice$',
        views.json_choice, name="json_choice"),
]
