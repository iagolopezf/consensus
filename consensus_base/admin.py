# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from django.contrib import messages
from django.contrib.admin import SimpleListFilter
from django.core.exceptions import PermissionDenied
from django.http import HttpResponse
from django.template.response import TemplateResponse
from django.utils.translation import ugettext as _

import csv
import datetime
import io
import pytz
import time
from jsonfield import JSONField
from jsoneditor.forms import JSONEditor

from .forms import UploadFileForm
from .models import Poll
from .models import PollChange
from .models import PollParticipant
from .models import PollQuestion
from .models import PollQuestionAnswer
from .models import PollQuestionComment

from .utils import get_model_field_names, \
    get_model_fields_values, from_tuple_to_model_by_id


def export_as_csv(self, request, queryset):
    # Create the HttpResponse object with the appropriate CSV header.
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = \
        'attachment; filename=%s-%s-export-%s.csv' % (
        __package__.lower(),
        queryset.model.__name__.lower(),
        datetime.datetime.now().strftime('%Y-%m-%d-%H-%M-%S'),
    )

    writer = csv.writer(response)
    first_iter = True
    for o in queryset:
        if first_iter:
            headers = get_model_field_names(o.__class__())
            writer.writerow(headers)
            first_iter = False
        values = get_model_fields_values(o)
        writer.writerow(values)
    return response


def import_from_csv(modeladmin, request, queryset):
    opts = modeladmin.model._meta
    app_label = opts.app_label

    # Check that the user has delete permission for the actual model
    if (
        not request.user.is_superuser and not
        modeladmin.has_delete_permission(request)
    ):
        raise PermissionDenied

    # The user has already confirmed the deletion.
    # Do the deletion and return a None to display the change list view again.
    if request.POST.get('post'):
        # print request.POST
        form = UploadFileForm(request.POST, request.FILES)
        if form.is_valid():
            csvfile = request.FILES['file']
            decoded_file = csvfile.read().decode('utf-8')
            io_string = io.StringIO(decoded_file)
            csvreader = csv.reader(io_string)
            first_iter = True
            field_names = []
            for row in csvreader:
                if first_iter:
                    field_names = row
                    first_iter = False
                else:
                    from_tuple_to_model_by_id(field_names, row, queryset.model)
            # return None
            modeladmin.message_user(request, _("Successfully imported."),
                                    messages.SUCCESS)
        # Return None to display the change list page again.
        return None

    title = _("Import")
    form = UploadFileForm()

    context = {
        "title": title,
        "form": form,
        'queryset': queryset,
        "opts": opts,
        "app_label": app_label,
    }

    # Display the confirmation page

    return TemplateResponse(request, "admin/%s/import_action.html" % app_label,
                            context)


class PollAdmin(admin.ModelAdmin):
    date_hierarchy = 'startdate'
    ordering = ['-startdate']
    list_editable = ['title', 'published', 'public', 'startdate', 'enddate']
    list_display = []
    list_display = list_display + list_editable + ['edit_html']
    list_filter = ['public', 'published']
    list_display_links = ['edit_html']
    list_per_page = 100

    def get_readonly_fields(self, request, obj=None):
        return ('slug',) + self.readonly_fields

    def edit_html(self, queryset):
        return '''<a href="%s/">Edit</a>''' % queryset.id
    edit_html.short_description = ''
    edit_html.allow_tags = True


class PollParticipantAdmin(admin.ModelAdmin):
    ordering = ['poll', 'username']
    list_editable = ['admin']
    list_display = ['poll', 'username']
    list_display = list_display + list_editable
    list_filter = ['username']
    list_per_page = 100

    # def get_readonly_fields(self, request, obj=None):
    #     return ('userid',) + self.readonly_fields


class PollQuestionAdmin(admin.ModelAdmin):
    ordering = ['poll', 'question']
    list_editable = ['hide', 'mandatory' ]
    list_display = ['question', 'poll']
    list_display = list_display + list_editable + ['edit_html']
    list_filter = ['hide', 'mandatory', 'typeofquestion']
    list_display_links = ['question', 'edit_html']
    list_per_page = 100

    def get_readonly_fields(self, request, obj=None):
        return ('slug',) + self.readonly_fields

    def edit_html(self, queryset):
        return '''<a href="%s/">Edit</a>''' % queryset.id
    edit_html.short_description = ''
    edit_html.allow_tags = True


class PollQuestionAnswerAdmin(admin.ModelAdmin):
    date_hierarchy = 'mtime'
    ordering = ['-mtime']
    list_display = ['question', 'mtime', 'username' ]
    list_filter = ['username']
    list_per_page = 100


class PollQuestionCommentAdmin(admin.ModelAdmin):
    date_hierarchy = 'mtime'
    ordering = ['-mtime']
    list_display = ['question', 'mtime', 'username' ]
    list_filter = ['username']
    list_per_page = 100


admin.site.register(Poll, PollAdmin)
admin.site.register(PollChange)
admin.site.register(PollParticipant, PollParticipantAdmin)
admin.site.register(PollQuestion, PollQuestionAdmin)
admin.site.register(PollQuestionAnswer, PollQuestionAnswerAdmin)
admin.site.register(PollQuestionComment, PollQuestionCommentAdmin)
