import calendar
import dateutil.parser
import pytz
import ldap
import re
from django_auth_ldap.config import LDAPSearch
from django_auth_ldap.backend import LDAPBackend
from django.conf import settings as s
from django.core.paginator import EmptyPage, PageNotAnInteger
from django.template.defaultfilters import slugify
from uuid import UUID


def sort_by(view):
    def _inner(request, *args, **kwargs):
        request.sort_by = request.GET.get('sortby', None)
        return view(request, *args, **kwargs)
    return _inner


def paginator_page(paginator, page):
    try:
        r = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        r = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        r = paginator.page(paginator.num_pages)
    return r


def ldap_populate_user(user):
    b = LDAPBackend()
    u = b.populate_user(user.username)
    return u


def ldap_populate_username(username):
    b = LDAPBackend()
    u = b.populate_user(username)
    return u


def ldap_groups(sorted=False):
    backend = LDAPBackend()
    connection = backend.ldap.initialize(s.AUTH_LDAP_SERVER_URI)
    search = s.AUTH_LDAP_GROUP_SEARCH
    result = search.execute(connection)
    whitelist = getattr(s, "AUTH_LDAP_GROUP_SEARCH_WHITELIST", None)
    if whitelist:
        return list(filter(lambda x: x in whitelist, map(lambda x: x[1]['cn'][0], result)))
    result = list(map(lambda x: x[1]['cn'][0], result))
    if sorted:
        result.sort()
    return result


def ldap_group_members(groupname):
    backend = LDAPBackend()
    connection = backend.ldap.initialize(s.AUTH_LDAP_SERVER_URI)
    search = s.AUTH_LDAP_GROUP_MEMBERS_SEARCH
    _,result = search.execute(connection, {'group': groupname})[0]
    return list(map(lambda x: x.split(',')[0].split('=')[1], result['uniquemember'][1:]))


def ldap_users(sorted=False):
    backend = LDAPBackend()
    connection = backend.ldap.initialize(s.AUTH_LDAP_SERVER_URI)
    search = s.AUTH_LDAP_USERS_SEARCH
    result = search.execute(connection)
    blacklist = getattr(s, "AUTH_LDAP_USERS_SEARCH_BLACKLIST", [])
    result = list(filter(lambda x: not x in blacklist, map(lambda x: x[1]['uid'][0], result)))
    if sorted:
        result.sort()
    return result


def from_date_string_to_timestamp(date_txt):
    return int(calendar.timegm(dateutil.parser.parse(date_txt).utctimetuple()))


def from_date_to_timestamp(date):
    return int(calendar.timegm(date.astimezone(pytz.utc).utctimetuple()))


def get_model_fields(model):
    return model._meta.fields


def get_model_field_names(model):
    res = []
    for field in get_model_fields(model):
        f = field.name
        res.append(f)
    return res


def get_model_fields_values(modelobj):
    res = []
    for field in get_model_fields(modelobj):
        f = str(getattr(modelobj, field.name))
        res.append(f)
    return res


def is_valid_uuid(uuid):
    try:
        uuid_obj = UUID(uuid)
    except:
        return False
    return uuid_obj.hex == uuid


def from_tuple_to_model_by_id(field_names, fields, model):
    i = 0
    values = {}
    for value in fields:
        values[field_names[i]] = value
        i += 1

    rs_found = False
    if "id" in values:
        for rs in model.objects\
                .filter(id=values["id"]).iterator():
            rs_found = True
            for k, v in values.items():
                if k != "id":
                    setattr(rs, k, v)
            rs.save()
    if not rs_found:
        rs = model()
        for k, v in values.items():
            if k != "id":
                setattr(rs, k, v)
        rs.save()


def unique_slugify(instance, value, slug_field_name='slug', queryset=None,
                   slug_separator='-'):
    """
    Calculates and stores a unique slug of ``value`` for an instance.

    ``slug_field_name`` should be a string matching the name of the field to
    store the slug in (and the field to check against for uniqueness).

    ``queryset`` usually doesn't need to be explicitly provided - it'll default
    to using the ``.all()`` queryset from the model's default manager.
    """
    slug_field = instance._meta.get_field(slug_field_name)

    slug = getattr(instance, slug_field.attname)
    slug_len = slug_field.max_length

    # Sort out the initial slug, limiting its length if necessary.
    slug = slugify(value)
    if slug_len:
        slug = slug[:slug_len]
    slug = _slug_strip(slug, slug_separator)
    original_slug = slug

    # Create the queryset if one wasn't explicitly provided and exclude the
    # current instance from the queryset.
    if queryset is None:
        queryset = instance.__class__._default_manager.all()
    if instance.pk:
        queryset = queryset.exclude(pk=instance.pk)

    # Find a unique slug. If one matches, at '-2' to the end and try again
    # (then '-3', etc).
    next = 2
    while not slug or queryset.filter(**{slug_field_name: slug}):
        slug = original_slug
        end = '%s%s' % (slug_separator, next)
        if slug_len and len(slug) + len(end) > slug_len:
            slug = slug[:slug_len-len(end)]
            slug = _slug_strip(slug, slug_separator)
        slug = '%s%s' % (slug, end)
        next += 1

    setattr(instance, slug_field.attname, slug)


def _slug_strip(value, separator='-'):
    """
    Cleans up a slug by removing slug separator characters that occur at the
    beginning or end of a slug.

    If an alternate separator is used, it will also replace any instances of
    the default '-' separator with the new separator.
    """
    separator = separator or ''
    if separator == '-' or not separator:
        re_sep = '-'
    else:
        re_sep = '(?:-|%s)' % re.escape(separator)
    # Remove multiple instances and if an alternate separator is provided,
    # replace the default '-' separator.
    if separator != re_sep:
        value = re.sub('%s+' % re_sep, separator, value)
    # Remove separator from the beginning and end of the slug.
    if separator:
        if separator != '-':
            re_sep = re.escape(separator)
        value = re.sub(r'^%s+|%s+$' % (re_sep, re_sep), '', value)
    return value
