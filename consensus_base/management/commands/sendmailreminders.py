from django.conf import settings as s
from django.core.management.base import BaseCommand, CommandError
from django.core.mail import send_mail

from consensus_base.helpers import get_participants_pending
from consensus_base.models import Poll
from consensus_base.utils import ldap_populate_username


class Command(BaseCommand):
    help = 'Send reminders for the peding votes'

    def add_arguments(self, parser):
        parser.add_argument(
            '--poll',
            dest='poll',
            help='Run for a specific poll',
        )

    def handle(self, *args, **options):
        polls = Poll.objects.filter(locked=False, published=True)
        if options['poll']:
            polls = polls.filter(uuid=options['poll'])

        for poll in polls:
            for participant in get_participants_pending(poll.id)[1]:
                try:
                    sent_reminder(poll, participant)
                except Poll.DoesNotExist:
                    self.stdout.write(self.style.ERROR('Problem sending reminder to %s for %s' % (participant, poll)))
            self.stdout.write(self.style.SUCCESS('Reminders sent for "%s"' % poll))


def sent_reminder(poll, username):
    user = ldap_populate_username(username)
    poll_url = getattr(s, "CONSENSUS_MAIL_POLL_URL_PATTERN", "%s") % poll.slug
    sender = getattr(s, "CONSENSUS_MAIL_SENDER", "noreply@consensus.local")
    send_mail(
    "[POLL] Pending votes in '%s'" % poll,
    """Hello %s,

we have noticed that you have pendings votes in '%s'. Please complete
the poll with your choices visiting this URL:

    %s

Thanks!!!
""" % (user.get_short_name(), poll, poll_url),
    sender,
    [user.email],
    fail_silently=False,
)
