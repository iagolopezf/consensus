from functools import reduce

from .utils import from_date_to_timestamp
from .utils import ldap_group_members

from .models import Poll
from .models import PollChange
from .models import PollGroupParticipant
from .models import PollParticipant
from .models import PollQuestion
from .models import PollQuestionAnswer
from .models import PollQuestionComment

def create_poll_change(poll, changename, value={}):
    poll_change = PollChange()
    poll_change.poll = poll
    change = {}
    change['type'] = changename
    change['value'] = value
    change['overall'] = get_overall_status(poll.id)
    poll_change.change = change
    poll_change.save()
    return poll_change


def get_participants(pollid):
    l = list(map(lambda x: x.username, 
                    PollParticipant.objects.filter(poll=pollid)
    ))
    l.sort()
    n = len(l)
    return n,l


def get_num_of_participants(pollid):
    return PollParticipant.objects.filter(poll=pollid).count()


def get_num_of_answers(question, username=None):
    q = PollQuestionAnswer.objects.filter(question=question)
    if username:
        q = q.filter(username=username)
    return q.values('userid').count()


def get_participants_pending(pollid, question=None):
    n = 0
    l = []
    poll_participant = PollParticipant.objects.filter(poll=pollid)
    for p in poll_participant:
        if question:
            poll_questions = [question]
        else:
            poll_questions = PollQuestion.objects.filter(poll=pollid, hide=False)
        for q in poll_questions:
            if not q.mandatory:
                continue
            a = q.pollquestionanswer_set.filter(username=p.username)
            if len(a) == 0:
                n += 1
                l.append(p.username)
                break
    l.sort()
    return n,l


def get_questions_pending(pollid, username=None):
    n = 0
    l = []
    poll_questions = PollQuestion.objects.filter(poll=pollid, hide=False)
    for q in poll_questions:
        if q.mandatory:
            if username:
                num_of_participants = 1
            else:
                num_of_participants = get_num_of_participants(pollid)
            num_of_answers = get_num_of_answers(q, username)
            if num_of_answers < num_of_participants:
                n += 1
                l.append(q.uuid.hex)
    return n,l


def get_num_of_questions_total(pollid, mandatory=False):
    p = PollQuestion.objects.filter(poll=pollid, hide=False)
    if mandatory:
        p = p.filter(mandatory=True)
    return p.count()


def get_choices_status(question, userid=None):
    choices = question.choices.copy()
    for c in choices.keys():
        text = choices[c]
        choices[c] = {}
        choices[c]['text'] = text
        choices[c]['list_of_participants'] = []
        choices[c]['num_of_answers'] = 0

    answers = PollQuestionAnswer.objects.filter(question=question).order_by('mtime')
    for a in answers:
        if len(a.answer) < 1:
            continue
        for c in a.answer.keys():
            if c not in choices.keys():
                continue
            choices[c]['list_of_participants'].append(a.username)
            choices[c]['num_of_answers'] += 1

    return choices


def get_user_answer(question_uuid, userid):
    q_answer = PollQuestionAnswer.objects.filter(question__uuid=question_uuid,
                                                 userid=userid)
    if len(q_answer) > 0:
        return q_answer[0].answer
    return None


def get_question_participants(question):
    choices_status = get_choices_status(question)
    return reduce_choices_status_value(
        choices_status, 'list_of_participants')


def get_question_status(question, userid=None, show_comments=False):
    choices_status = get_choices_status(question, userid)
    status = {}
    status['text'] = question.question
    status['slug'] = question.slug
    status['uuid'] = question.uuid.hex
    status['typeofquestion'] = question.typeofquestion
    status['mandatory'] = question.mandatory
    status['hide'] = question.hide
    status['properties'] = question.properties
    status['choices'] = choices_status
    status['num_of_answers'] =  get_num_of_answers(question)
    status['list_of_participants'] = reduce_choices_status_value(
        choices_status, 'list_of_participants')
    if userid:
        q_answer = PollQuestionAnswer.objects.filter(question=question,
                                                     userid=userid)
        if len(q_answer) > 0:
            status['user_answer'] = q_answer[0].answer
    if show_comments:
        comments = []
        q_comments = PollQuestionComment.objects.filter(question=question).order_by('mtime')
        for comment in q_comments:
            c = get_comment_status(comment)
            comments.append(c)
        status['comments'] = comments
    return status


def get_comment_status(comment):
    c = {}
    c['comment'] = comment.comment
    c['mtime'] = from_date_to_timestamp(comment.mtime)
    c['username'] = comment.username
    c['uuid'] = comment.uuid.hex
    c['question_uuid'] = comment.question.uuid.hex
    return c


def get_overall_status(pollid):
    p = Poll.objects.filter(id=pollid)[0]

    num_of_questions_pending, list_of_questions_pending = \
        get_questions_pending(pollid)
    num_of_participants_pending, list_of_participants_pending = \
        get_participants_pending(pollid)
    return {
        'title': p.title,
        'description': p.description,
        'locked': p.locked,
        'num_of_participants': get_num_of_participants(pollid),
        'num_of_questions_total': get_num_of_questions_total(pollid),
        'num_of_mandatory_questions_total': get_num_of_questions_total(pollid, mandatory=True),
        'num_of_questions_pending': num_of_questions_pending,
        'num_of_participants_pending': num_of_participants_pending,
        'list_of_questions_pending': list_of_questions_pending,
        'list_of_participants_voted': list(filter(lambda x: x not in list_of_participants_pending, get_participants(pollid))),
        'list_of_participants_pending': list_of_participants_pending,
    }


def get_questions_status(pollid, userid=None, show_comments=False):
    res = []
    poll_questions = PollQuestion.objects.filter(poll=pollid).order_by('id')
    for q in poll_questions:
        status = get_question_status(q,userid,show_comments)
        res.append(status)
    return res


def is_participant(poll, username):
    if poll.public:
        return True
    poll_participant = PollParticipant.objects.filter(username=username,
                                                      poll=poll)
    return len(poll_participant) > 0


def is_admin(poll, username):
    poll_participant = PollParticipant.objects.filter(username=username,
                                                      poll=poll, admin=True)
    return (poll.creator == username) or len(poll_participant) > 0


def is_poll_visible_by_user(poll, username):
    if poll.published and is_participant(poll, username):
        return True
    if is_admin(poll, username):
        return True
    return False


def reduce_choices_status_value(choices_status, keyname):
    if len(choices_status) == 0:
        return []
    res  = list(set(reduce (lambda x,y: x + y,
                            map(lambda x: x[keyname],
                choices_status.values()))))
    return res

def set_participants(poll, groups, users):
    groups = list(groups)
    users = list(users)
    for g in groups:
        users += ldap_group_members(g)
    # Ensure all participants are added, even the newest
    list(map(lambda x: PollParticipant.objects.get_or_create(poll=poll, username=x), users))
    # Remove not longer selected participants:
    PollParticipant.objects.filter(poll=poll).exclude(username__in=users).delete()
    # Ensure all groups assigned are added
    list(map(lambda x: PollGroupParticipant.objects.get_or_create(poll=poll, group=x), groups))
    # Remove not longer selected groups
    PollGroupParticipant.objects.filter(poll=poll).exclude(group__in=groups).delete()
