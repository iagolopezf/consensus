# flake8: noqa
# {{ ansible_managed }}

ALLOWED_HOSTS = [
    "{{ djangoconsensusapp_server_name }}"
]

{{ djangoconsensusapp_django_consensus_local_settings_custom }}
