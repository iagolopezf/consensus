# django_consensus

## Quick start

Setting up the enviroment::

``` sh
wget https://bootstrap.pypa.io/get-pip.py
python get-pip.py
apt install build-essential libsasl2-dev python-dev libldap2-dev libssl-dev
apt install python3-dev 
# apt install python-dev   # python 2 
pip install virtualenv

# clone django_consensus repository
git clone https://gitlab.com/saavedra.pablo/consensus.git
cd consensus
virtualenv -p python3  env
source env/bin/activate
pip install -r requirements.txt.python3

# Setup
python manage.py migrate
python manage.py creatersakey

# ...
./manage.py runsslserver 0.0.0.0:8443

# ...
deactivate
```


Any kind of SSL errors like `ssl.SSLEOFError: EOF occurred in violation of
protocol (_ssl.c:1949)`?  Try with this:

``` sh
pip install --force-reinstall requests[security]
```
Ref: https://github.com/jakubroztocil/httpie/issues/315


## Updating the requirements

```
pip freeze > requirements.txt
```

## Project directory layout


```
  django_consensus_site
    __init__.py
    settings.py
    urls.py
    wsgi.py
  app1
    migrations
    __init__.py
    admin.py
    apps.py
    models.py
    tests.py
    views.py
  static
    css
      main.cs
    js
      main.js
  manage.py
```

The django_consensus_site has a base template for the project (`base.html`) with the 
header and footer and a `{%block content%}` for your main content.

Have your other templates inherit form base.html 
`{% extends "base.html" %}` and override the content section.

As long as the apps are in `INSTALLED_APPS` and the template loader for apps 
dirs  is enabled, we can include any template from another app:

```
{% include "header.html" %}
```

The templates are located directly in the templates dir of the apps. Generally, 
in order to avoid name clashes it is better to use:


```
app1/
  templates/
app1/
  page1.html
  page2.html
app2/
  templates/
app2/
  page1.html
  page2.html
```

And `{% include "app1/page1.html" %}` or  `{% include "app2/page1.html" %}`.


## Develop

* `<feature>`: Branch dedicated to a especific branch. Must be synced with
  `develop`
* `<username>`: Integration area for each developer (many features can be
  included in each developer branch). Must be synced with `develop`
* `ci`: Continuous integration branch. All the code and features which they
  has a considerable level of maturity. You are interested in test your code
  with other on going features.
* `develop`: Project integration stage. Usually is the consolidation from each
  developer branch and or feature.
  * The code must accomplish with the unit tests before to be merged here
* `master`:  Code ready to *production* stage. Usually is a merge from `develop`.
  * Only code which passes all the suite test can reach this stage  
